import sys
import os
import json
import base64
import time
import requests
import re
from pathlib import Path


def download_segment(base_url: str, clip_id: str, segment: object) -> bytes:
    url = f"{base_url}/{clip_id}/parcel/video/{segment['url']}"
    print(url)
    response = requests.get(url)
    if not response.ok:
        print(response)
        exit(1)
    return response.content


def read_master(url: str) -> dict:
    print(f"Retrieving master.json from {url}")
    response = requests.get(url)
    if not response.ok:
        print(response)
        exit(1)

    return json.loads(response.content)


def video_by_height(master: dict, target_height: int) -> object:
    # There are several video resolutions.
    # We want the highest one, which is 720p (DVD quality)
    videos = master['video']
    print(f"len videos = {len(videos)}")
    for v in videos:
        height: int = int(v['height'])
        print(f"Id: {v['id']}; Height: {height}")
        if height == target_height:
            print(f"Downloading video of height = {height}")
            return v
    print(f"Specified video height is not found: {target_height}")
    exit(1)

def main() -> int:
    if len(sys.argv) < 2:
        print("Usage:  rip.py [mp4 basename] [master json url] <video height>")
        return 1

    master_filename: str = sys.argv[1]                      # e.g. 'wednesday-concert'
    mp4_filename: str = Path(master_filename).stem + '.mp4' # e.g. 'wednesday-concert.mp4'

    # if e.g.         "https://175vod-adaptive.akamaized.net/exp=1633611384~acl=/53b0102a-a535-4d7a-9d9e-3d848e05628e/*~hmac=6699a613a9c33292af788b091a31f8f4fa402d9887d56f4c6777aaf5779d023e/53b0102a-a535-4d7a-9d9e-3d848e05628e/video/8806fe65,bfe83e1b,62cc10ea,ad9b8168,76dbe0c6/master.json?query_string_ranges=1&base64_init=1"
    # then base_url = "https://175vod-adaptive.akamaized.net/exp=1633611384~acl=/53b0102a-a535-4d7a-9d9e-3d848e05628e/*~hmac=6699a613a9c33292af788b091a31f8f4fa402d9887d56f4c6777aaf5779d023e"
    master_json_url: str = sys.argv[2]
    search: object = re.search("(https:.*hmac=.*?)/", master_json_url)
    if not search:
        print("Invalid master json url")
    base_url = search.group(1)

    video_height = int(sys.argv[3]) if len(sys.argv) >= 4 else 1080

    # Parse master.json (MIME type of application/json)
    # Cache json constantly accessed
    master: dict = read_master(master_json_url)
    clip_id = master['clip_id']
    video = video_by_height(master, video_height)
    video_segments = video['segments']
    num_segments = len(video_segments)

    # Check segment ranges are consecutive
    current_segment = 1
    expected_end = 6
    for seg in video_segments:
        if current_segment == len(video_segments):          # last segment gets a pass
            break
        current_segment = current_segment + 1

        if seg['end'] != expected_end:
            print(f"Unexpected segment end time: start ={seg['start']}; end = {seg['end']}")
            return 1
        expected_end = expected_end + (seg['end'] - seg['start'])

    print(f"Verified master.json contains consecutive and in-order segments")

    # Create output audio.m4a and video.m4v files
    with open(mp4_filename, 'wb') as mp4_file:
        # Write init segment - needs to be decoded using base64
        print("Writing Init segment")
        mp4_file.write(base64.b64decode(video['init_segment']))

        # Download and write all segments
        current_segment = 1
        for seg in video_segments:
            print(f"  Segment {current_segment:,} of {num_segments:,}")
            vseg = download_segment(base_url, clip_id, seg)
            mp4_file.write(vseg)
            # source:  https://stackoverflow.com/a/47172123/2073217
            mp4_file.flush()
            os.fsync(mp4_file.fileno())

            # wait between segments to lighten server load
            if current_segment % 4 == 0:
                time.sleep(1.5)

            current_segment = current_segment + 1


    '''
    # Download first video segment
    segment_url = get_segment_url(clip_id, 'video', video_id, 'segment-1.m4s')
    #print(segment_url)
    segment : bytes = download_segment(segment_url)
    print(segment.content)
    '''

if __name__ == "__main__":
    main()


