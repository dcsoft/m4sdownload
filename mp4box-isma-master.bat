REM Prepare any mp4 for ISMA streaming: MP4Box -isma -hint myfile.mp4
REM Source:  https://gpac.wp.imt.fr/mp4box/mp4box-documentation/
"c:\Program Files\GPAC\mp4box.exe" -isma -hint -add "%~1.m4v" -add "%~1.m4a" "%~1.mp4"