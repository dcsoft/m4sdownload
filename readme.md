# M4S Downloader

## Introduction

I had a time limited subscription to video clips that are played in a browser with a Vimeo player.  My goal was to to download the clips to my hard drive for future playback.

Though there are automated downloaders such as [youtube-dl](https://github.com/rg3/youtube-dl), they don't work on websites where the website is password protected.

My plan was to use the browser to login to the video website, access the videos, and see how the browser accessed them.  Then I would write a Python script to emulate that behavior.

Using the Chrome Developer Tools (open the Chrome browser and press F12), _Network_ tab, it shows the urls, HTTP requests, and HTTP responses that the Vimeo player is using.  It became evident that video playback was being streamed by downloading e.g. _segment-1.m4s_, _segment-2.m4s_, etc.  .M4S files are part of the _MPEG-DASH_ spec.  

> NOTE:  It is also helpful to disable caching in the Chrome Developer Tools so that the Network tab shows traffic that would otherwise have been cached.

Further, the segments for the audio and video were being downloaded from separate urls.  IOW, the audio and video were streaming from separate locations and combined by the player to play them.

A Google search reveals [Combine MPEG-DASH segments (ex, init.mp4 + segments.m4s) back to a full source.mp4?](https://stackoverflow.com/questions/23485759/combine-mpeg-dash-segments-ex-init-mp4-segments-m4s-back-to-a-full-source-m
) that indicates these files can be combined to create audio and video MPEG streams.

Then, the free utiity [M4Box](https://gpac.wp.imt.fr/mp4box/) is used to combine (i.e. multiplex/'mux'/"combine") the .m4a and .m4v files into a playable .mp4 file.



## EDIT 2021

It's simpler now:

1. The audio and video are now combined, so there is only one stream to download.
2. The .mp4 video is still downloaded in segments, but instead of each segment having a different url, they are specified by a RANGE parameter in the GET of the constant .mp4 url.
3. The _MP4Box_ utilities (which used to combine the audio and video segments) aren't required anymore, since the MP4 file is created directly.  They may still be used to add seekability for MP4's streamed from a server.


## Rip.py

Rip.py is a script that sequentially downloads start-end ranges of the .mp4, and writes them to the output .mp4 file.

    Usage:  rip.py [mp4 basename] [master json url]  <video height>
    Example:   rip.py 10-friday-concert https://180vod-adaptive.akamaized.net/exp=1633636921~acl=/fd4bc8f2-496d-41a3-8589-0964a74b84d3/*~hmac=c11055b9f2008548ca62a6e0c1ddb61ec66c938f5c4db28659f1aa90e0e9d661/fd4bc8f2-496d-41a3-8589-0964a74b84d3/video/9ad2f7c5,8d345829,4afc702f,7d25e70c,ef1e7285/




1. _mp4 basename_ is the desired filename of the output .mp4.
2. _master json url_ is the from the Chrome Developer Tools when the master.json file is selected and the Headers tab shows the Request URL.
![img.png](master_json.png)

    NOTE:  The **&** character circled in red above needs to be escaped by surrounding it with **"**

   ```
   master.json?query_string_ranges=1"&"base64_init=1
   ```
3. _video height_ specifies the video resolution to be downloaded.  It has to match one of the json objects in _master.json_ (typically 240, 360, 540, 720, 1080).  If omitted, the 1080 resolution is selected.   


The script creates the output .mp4 file, then appends the _init segment_ of the selected video resolution.  This specifies the metadata of the .mp4 file and is necessary for the .mp4 player to properly play the file, after download is complete.

The script downloads each segment range (typically about 6 seconds) of audio/video, sleeping periodically to avoid putting too much load on the server; this can be decreased to speed up the downloading, or increased to ease server load even more.  The downloaded segment is appended to the .mp4 file.  The file is flushed periodically so that the file size is updated in Explorer.


## Download.py

Download-mp4.py is a script that downloads the entire MP4 file in one request, but only works on the lower resolution videos (up to 360 vertical).

     Usage:  download.py [MP4 output filename] [url]
     Example:  download.py saturday-morning-goldcity_0360.mp4 https://100vod-adaptive.akamaized.net/exp=1633640111~acl=/73dfaae4-a2a7-4e55-9b23-bd50d4f918e3/*~hmac=f4ad8d9c56467807beabced3e2a60c05ce64b1d3941af612713ea853b3667c62/73dfaae4-a2a7-4e55-9b23-bd50d4f918e3/parcel/video/f864e69c.mp4

1. _MP4 output filename_ is the name of the created .mp4 file
2. _url_ is the Request url of the segment mp4 from Chrome Developer Tools, not including the ?range= parameters.

To use:  in the Vimeo player, use the Settings to selectt the desired video resolution (resolutions above 360 fail).  Then click Play to start playback.  In Chrome Developer Tools, click on one of the urls that has downloaded a segment.  It should have the form:

    https://59vod-adaptive.akamaized.net/exp=1633647453~acl=/4c49.../*~hmac=0d70e.../4c49.../parcel/video/57563a20.mp4?range=7694867741-7695731116

In this case, specify just the first part:

    https://59vod-adaptive.akamaized.net/exp=1633647453~acl=/4c49.../*~hmac=0d70e.../4c49.../parcel/video/57563a20.mp4


## M4Box

There are useful .bat files to alter the .mp4 for seekability.

### MP4Box.bat

Easily launch MP4Box.exe (not on the path) and forward command-line paramaters.

### MP4Box-master.bat

Create a .MP4 file containing specified audio and video.  The .m4a and .m4v files are assumed to have the same root filename, e.g.

    clip1.m4a
    clip1.m4v

    c:\> MP4Box-master.bat clip

    Produces:  clip.mp4


### MP4Box-isma-master.bat

Creates .mp4 capable of being copied to e.g. OneDrive and streamed in a browser (it adds -isma and -hint flags).  The .mp4 file grows to almost 2x as big, though.


### jsonpp.bat

Invokes Python to pretty-print a .json file.  This helps view the structure of _master.json_. 