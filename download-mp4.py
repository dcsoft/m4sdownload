import sys
import requests

#mp4_url = 'https://192vod-adaptive.akamaized.net/exp=1633355890~acl=/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/*~hmac=c8093203c468d4235d89a7ec8e06bcd285568dbdca8cf8ff2428ce7c33887858/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/parcel/video/e89f56cf.mp4'
#mp4_url = 'https://192vod-adaptive.akamaized.net/exp=1633448806~acl=/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/*~hmac=b5650652ebd7146a20954125df920129c430181417459c412794a96169e4f104/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/parcel/video/e89f56cf.mp4'

#240
#mp4_url  = 'https://192vod-adaptive.akamaized.net/exp=1633449349~acl=/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/*~hmac=7c409fae9a70266c93a1407411c20847971673de17efb4eb984c74632bb08543/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/parcel/video/2c068d4e.mp4'

#360
#mp4_url  = 'https://192vod-adaptive.akamaized.net/exp=1633449349~acl=/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/*~hmac=7c409fae9a70266c93a1407411c20847971673de17efb4eb984c74632bb08543/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/parcel/video/85c37f33.mp4'

#540 (failed)
#mp4_url  = 'https://192vod-adaptive.akamaized.net/exp=1633449349~acl=/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/*~hmac=7c409fae9a70266c93a1407411c20847971673de17efb4eb984c74632bb08543/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/parcel/video/8d45ba57.mp4'

#720 (failed)
#mp4_url  = 'https://192vod-adaptive.akamaized.net/exp=1633449349~acl=/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/*~hmac=7c409fae9a70266c93a1407411c20847971673de17efb4eb984c74632bb08543/6e6a31c1-4c2d-41f8-87e1-54acf1c6e1c7/parcel/video/da9b8c0b.mp4'


def download(url: str) -> bytes:
    return requests.get(url).content

def main():

    if len(sys.argv) < 2:
        print("Usage:  download-mp4.py [mp4 filename] [mp4 download url]")
        return 1

    mp4_filename: str = sys.argv[1]      # e.g. 'wednesday-concert'
    mp4_url: str = sys.argv[2]      # e.g. 'wednesday-concert'

    with open(mp4_filename, 'wb') as mp4_file:
        mp4_seg = download(mp4_url)
        mp4_file.write(mp4_seg)


if __name__ == "__main__":
    main()


